/**
 * @file
 * Global javascripts for the theme.
 */

(function ($) {
  Drupal.behaviors.superfish = {
    attach: function () {
      $(".slimmenu").slimmenu(
        {
          resizeWidth: '800',
          collapserTitle: 'Navigation',
          indentChildren: true,
          childrenIndenter: '&raquo;'
        });
    }
  }
})(jQuery);
